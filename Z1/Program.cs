﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("primjer.csv");
            Console.WriteLine("Original file data: \n" + dataset.ToString());

            Dataset cloneDataset = new Dataset();
            cloneDataset = (Dataset)dataset.Clone();
            Console.WriteLine("Cloned file data: \n" + cloneDataset.ToString());

            if(object.ReferenceEquals(dataset, cloneDataset))
                Console.WriteLine("Objects are the same.");
            else
                Console.WriteLine("Objects are not the same.");

            Console.ReadKey();
        }
    }
}
//Je li potrebno duboko kopiranje za klasu u pitanju?
    //U ovom slucaju nije potrebno duboko kopiranje jer se ono koristi kada imamo dinamicku alokaciju 
    //memorije (u slucaju pokazivaca).