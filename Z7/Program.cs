﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZAD7
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager Notification_Manager = new NotificationManager();

            ConsoleNotification Console_Notification = new ConsoleNotification("ERROR started it", "This is an Error", "It's erring", DateTime.Now, Category.ERROR, ConsoleColor.Red);
            Notification_Manager.Display(Console_Notification);

            Console_Notification = new ConsoleNotification("ALERT alerted", "This is an alert", "It's alerting you <.<", DateTime.Now, Category.ALERT, ConsoleColor.DarkYellow);
            Notification_Manager.Display(Console_Notification);

            Console_Notification = new ConsoleNotification("INFORMATION has been given", "This is information", "It tells you information", DateTime.Now, Category.INFO, ConsoleColor.DarkCyan);
            Notification_Manager.Display(Console_Notification);

            ConsoleNotification Cloned_Console_Notification = new ConsoleNotification();
            Notification_Manager.Display(Cloned_Console_Notification);

            Cloned_Console_Notification = (ConsoleNotification)Console_Notification.Clone();
            Notification_Manager.Display(Cloned_Console_Notification);

            Console.ReadKey();

        }
    }
}
