﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
           
            NotificationManager Notificatio_Manager = new NotificationManager();
            NotificationBuilder Notification_Builder = new NotificationBuilder();
            Notification_Builder.SetTitle("Alert!");
            Notification_Builder.SetAuthor("Notification_Builder");
            Notification_Builder.SetColor(ConsoleColor.Red);
            Notification_Builder.SetLevel(Category.ALERT);
            Notification_Builder.SetTime(DateTime.Now);
            Notificatio_Manager.Display(Notification_Builder.Build());

            Console.ReadKey();
        }
    }
}
